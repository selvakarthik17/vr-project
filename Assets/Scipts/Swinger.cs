using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swinger : MonoBehaviour
{
    [SerializeField] public GameObject player;
    [SerializeField] public GameObject swing;
    [SerializeField] public GameObject camera;
    [SerializeField] public GameObject exitBox;
    void Start()
    {
    }

    public void playerSwing()
    {
        player.transform.position = new Vector3(23, 1, 11);
        player.transform.parent = swing.transform;

        Vector3 newRotation = new Vector3(0, -45, 0);
        camera.transform.eulerAngles = newRotation;

        exitBox.SetActive(true);
    }
}