using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateDoorLeft : MonoBehaviour
{
    [SerializeField] public GameObject Door;
    [SerializeField] public GameObject DoorCover;
    private Quaternion DoorOpen;
    private Quaternion DoorClosed;
    private float smooth = 10;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    bool isOpen = false;

    public void toggleDoor()
    {
        if(isOpen)
        {
            closeDoor();
        }
        else
        {
            openDoor();
        }

        isOpen = !isOpen;
    }

    private void openDoor()  
    {
        Vector3 newRotation = new Vector3(-90, -90, 180);
        transform.eulerAngles = newRotation;

    }
    private void closeDoor()
    {
        Vector3 newRotation = new Vector3(-90, -180, 180);
        transform.eulerAngles = newRotation;
    }
}
