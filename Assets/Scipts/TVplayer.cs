using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class TVplayer : MonoBehaviour
{
    [SerializeField] VideoPlayer videoplayer;
    [SerializeField] Material white;
    [SerializeField] Material black;
    [SerializeField] GameObject cube;

    bool isPlay = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toggleTV()
    {
        if(!isPlay)
        {
            cube.GetComponent<MeshRenderer>().material = white;
            videoplayer.Play();
        }
        else
        {
            cube.GetComponent<MeshRenderer>().material = black;
            videoplayer.Stop();
        }

        isPlay = !isPlay;
    }

}
