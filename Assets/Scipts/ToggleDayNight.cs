using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleDayNight : MonoBehaviour
{
    [SerializeField] public Light sun;

    [SerializeField] public Light g_a;
    [SerializeField] public Light g_b;
    [SerializeField] public Light g_c;

    [SerializeField] public Light f_a;
    [SerializeField] public Light f_b;
    [SerializeField] public Light f_c;

    [SerializeField] public Light s_a1;
    [SerializeField] public Light s_b1;
    [SerializeField] public Light s_c1;
    [SerializeField] public Light s_d1;

    [SerializeField] public Light s_a2;
    [SerializeField] public Light s_b2;
    [SerializeField] public Light s_c2;
    [SerializeField] public Light s_d2;

    [SerializeField] public Light s_s1;
    [SerializeField] public Light s_s2;
    [SerializeField] public Light s_s3;
    [SerializeField] public Light s_s4;

    [SerializeField] public GameObject toggleBox;
    bool isDay = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toggleDayNight()
    {   
        if(isDay)
        {
            sun.GetComponent<Light>().intensity = 0;
            Vector3 newRotation = new Vector3(191, -710, 287);
            sun.transform.eulerAngles = newRotation;

            g_a.enabled = true;
            g_b.enabled = true;
            g_c.enabled = true;

            f_a.enabled = true;
            f_b.enabled = true;
            f_c.enabled = true;

            s_a1.enabled = true;
            s_b1.enabled = true;
            s_c1.enabled = true;
            s_d1.enabled = true;

            s_a2.enabled = true;
            s_b2.enabled = true;
            s_c2.enabled = true;
            s_d2.enabled = true;

            s_s1.enabled = true;
            s_s2.enabled = true;
            s_s3.enabled = true;
            s_s4.enabled = true;

        }
        else
        {
            sun.GetComponent<Light>().intensity = 1;
            Vector3 newRotation = new Vector3(124, -439, 276);
            sun.transform.eulerAngles = newRotation;

            g_a.enabled = false;
            g_b.enabled = false;
            g_c.enabled = false;

            f_a.enabled = false;
            f_b.enabled = false;
            f_c.enabled = false;

            s_a1.enabled = false;
            s_b1.enabled = false;
            s_c1.enabled = false;
            s_d1.enabled = false;

            s_a2.enabled = false;
            s_b2.enabled = false;
            s_c2.enabled = false;
            s_d2.enabled = false;

            s_s1.enabled = false;
            s_s2.enabled = false;
            s_s3.enabled = false;
            s_s4.enabled = false;
        }

        isDay = !isDay;

    }
}
