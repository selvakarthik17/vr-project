using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReticleAnimator : MonoBehaviour
{
    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();   
    }
    void Update()
    {
        
    }

    public void setOpen()
    {
        anim.SetBool("isOpen",true);
    }

    public void setClose()
    {
        anim.SetBool("isOpen",false);
    }
}
