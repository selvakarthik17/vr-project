using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class exitScript : MonoBehaviour
{
    [SerializeField] public GameObject player;
    [SerializeField] public GameObject camera;
    [SerializeField] public GameObject exitBox;

    void Start()
    {

    }

    void Update()
    {

    }

    public void exitSwing()
    {
        player.transform.parent = null;
        Vector3 newRotation1 = new Vector3(0, 0, 0);
        player.transform.eulerAngles = newRotation1;
        Vector3 newRotation = new Vector3(0, 135, 0);
        camera.transform.eulerAngles = newRotation;
        exitBox.SetActive(false);
    }
}